FROM node:12
WORKDIR /watermelontest
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
EXPOSE 6379
CMD [ "node", "app.js" ]