const localhost = '127.0.0.1';
const redisPort = '6379';
const port = '3000';
const express = require('express');
const app = express();
const redis = require('redis');
const client = redis.createClient(); // host, port
const fetch = require('node-fetch');
var path = require('path');
const nocache = require('nocache');

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
app.use(express.static("src"));
app.use(nocache());
app.disable('view cache');
app.use(express.json());
app.use(express.urlencoded());

client.on('error', (err) => {
    console.log('Error :' + err);
});

app.get('/', (req, res) => {
    res.redirect('/login');
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, './src', 'login.html'));
});

app.post('/login', (req, res) => {
    if (req.body && req.body != {}){
        var user = req.body.user;
        var password = req.body.password;
        var errorcode = 0; // 0 no hay error, 1, usuario no encontrado, 2 password no valido
        client.get('user:'+user, (error, result) => {
            if (error) {
                errorcode = 1
            }
            if (password != result) {
                errorcode = 2
            }
            if (!errorcode) {
                client.setex("login:"+user,120,1, (end) => {
                    var redirUrl = req.protocol + '://' + req.get('host') + '/visualizer?user=' + encodeURIComponent(user);
                    return res.send({success: true, url: redirUrl}); //Se envia url en vez de redireccionar. El redireccionamiento se hace local
                }); //2 minutos
            }
        });
    }   
});
app.get('/register', (req, res) => {
    res.sendFile(path.join(__dirname, './src', 'register.html'));
});

app.post('/register', (req, res) => {
    if (req.body && req.body != {}){
        var user = req.body.user;
        var password = req.body.password;
        client.set("user:"+user,password);
        res.send('Registered');
    }
});

app.get('/visualizer', (req, res) => {
    var user = req.query.user;
    client.get("login:"+user, (error, result) => {
        if (result == null || error) {
            return res.redirect('/login'); //Este redireccionamiento si se puede hacer en servidor
        }
    });
    const apiurl = "https://rickandmortyapi.com/api/character/"
    var characters = [];
    var urls = [];
    for (i = 1; i<10; i++){
        urls = urls.concat(apiurl + i);
    }
    Promise.all(urls.map(url =>
        fetch(url, {method: "Get"})
            .then(res => {return res.json();})
    )).then(allRes => {
        var tempChar = {}
        allRes.forEach(json => { //Construir nueva lista de data, con solo lo necesario
            var character = {};
            character.name = json.name;
            character.status = json.status;
            character.species = json.species;
            character.gender = json.gender;
            character.image = json.image;
            characters.push(character);
        });
        res.cookie('data', JSON.stringify(characters));
        res.sendFile(path.join(__dirname, './src', 'visualizer.html'));
    });
});

app.get('/test', (req, res) => {
    const apiurl = "https://rickandmortyapi.com/api/character/"
    var characters = [];
    var urls = []
    for (i = 1; i<10; i++){
        urls = urls.concat(apiurl + i);
    }
    Promise.all(urls.map(url =>
        fetch(url, {method: "Get"})
            .then(res => {return res.json();})
    )).then(allRes => {
        var tempChar = {}
        allRes.forEach(json => {
            var character = {};
            character.name = json.name;
            character.status = json.status;
            character.species = json.species;
            character.gender = json.gender;
            character.image = json.image;
            characters.push(character);
        });
        res.cookie('data', JSON.stringify(characters));
        res.sendFile(path.join(__dirname, './src', 'visualizer.html'));
    });
});

app.get('/testregister', (req, res) => {
    client.set("user:testuser","testpassword");
    res.send('Register test');
});