'use strict';

const e = React.createElement;

class Register extends React.Component {
    
    constructor(props) {
        super(props);     
    }

    login() {
        $.ajax({
            method: 'POST',
            url : 'login',
            data: {'user' : $('#txtName').val(), 'password' : $('#txtPassword').val()}
        }).done(function(response){
            if(response.success){
                window.location.replace(response.url);
            }
        });
    }
    
    render() {
        return(React.createElement('button', {variant: 'contained', id:'btnLogin', onClick: () => this.login() }, 'Login' ))
    }
}
const container = document.querySelector('#divReactLogin');
ReactDOM.render(e(Register), container);