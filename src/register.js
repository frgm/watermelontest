'use strict';

const e = React.createElement;

class Register extends React.Component {
    
    constructor(props) {
        super(props);     
    }

    registerUser() {
        $.ajax({
            method: 'POST',
            url : 'register',
            data: {'user' : $('#txtName').val(), 'password' : $('#txtPassword').val()}
        }).done(function(response){
            if(response.success){
                console.log('OK');
            }
        });
    }
    
    render() {
        return(React.createElement('button', {variant: 'contained', id:'btnRegistrar', onClick: () => this.registerUser() }, 'Registrar' ))
    }
}
const container = document.querySelector('#divReactRegister');
ReactDOM.render(e(Register), container);