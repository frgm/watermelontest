'use strict';

const e = React.createElement;

class DataParser extends React.Component {
    
    constructor(props) {
        super(props);     
    }

    render() {
        //console.log(decodeURIComponent(document.cookie));
        const data = JSON.parse(decodeURIComponent(document.cookie).split('=')[1])
        var parsed = data.map(character => {
                return (
                    Object.values(character).map(value => {
                        if(value.split(':')[0]=='https'){
                            return ( React.createElement('img', {src: value}, null ))
                        }else{
                            return ( React.createElement('p', null, value ))
                        }
                    }) 
                );
        })
        return(parsed)
    }
}
const container = document.querySelector('#divReactVisualizer');
ReactDOM.render(e(DataParser), container);